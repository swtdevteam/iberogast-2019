
app.register('ap-auto-references-popup', function () {
	var REF_ATTR = 'data-reference-id';
	var PRESENTATION_ID = '#presentation';
	var SCROLL_CLASS = 'scroll';
	var REF_POPUP_CLASS = 'auto-references-popup';
	var REF_POPUP_OVERLAY_CLASS = 'auto-references-popup-overlay';
	var REF_LIST_CLASS = 'references';
	var HIDE_CLASS = 'hidden';
	var CLOSE_EL_CLASS = 'x';
	var CLOSE_ICON = '⊗';
	var REF_SELECTOR = '[' + REF_ATTR + ']';
	var CLOSE_EL_SELECTOR = '.' + REF_POPUP_CLASS + ' .' + CLOSE_EL_CLASS;
	var REF_STR = 'References';

	return {
		publish: {},
		events: {},
		states: [],

		onRender: function () {
			this.$scroll = $('<div class=' + SCROLL_CLASS + '/>');
			this.$list = $('<ul class=' + REF_LIST_CLASS + '/>');
			this.$popup = $('<div class=' + REF_POPUP_CLASS + '/>')
				.append('<header><div class=' + CLOSE_EL_CLASS + '>'
					+ CLOSE_ICON+ '</div><h1>' + REF_STR + '</h1></header>');
			this.$overlay = $('<div/>',
				{'class': REF_POPUP_OVERLAY_CLASS + ' ' + HIDE_CLASS});
			this.refindexesList = {};

			this.$scroll.append(this.$list);
			this.$popup.append(this.$scroll);
			this.$overlay.append(this.$popup)
				.on("swipedown swipeup swiperight swipeleft", function (e) {
					e.stopPropagation();
				})
				.on("tap", function (event) {
					if ($(event.target).is(CLOSE_EL_SELECTOR))
						$(this).addClass(HIDE_CLASS);
				}).appendTo(PRESENTATION_ID);

			app.listenTo(app.slide, 'slide:enter',
				this.autoReferencePopupHandler.bind(this));
			app.on('refindexes:created', this.addRefindexes.bind(this));
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},

		addRefindexes: function(refindexes, slideID) {
			refindexes = refindexes.sort(function(a, b) {
				return a > b ? 1 : -1;
			});
			this.refindexesList[slideID] = refindexes;
		},

		getMediaResources: function (ids, mediaData) {
			var references = {};
			$.each(mediaData, function (file, meta) {
				if (ids && ids.indexOf(meta.referenceId) > -1) {
					references[file] = meta;
				}
			});
			return references;
		},

		autoReferencePopupHandler: function (data) {
			var $slide = $('#' + data.id);
			var self = this;
			var mediaData = window.mediaRepository.metadata();
			var localRefindexes = self.refindexesList[data.id];
			var references = this.getMediaResources(localRefindexes, mediaData);

			this.$overlay.addClass(HIDE_CLASS);
			$slide.find(REF_SELECTOR)
				.off('tap.' + REF_POPUP_CLASS)
				.on('tap.' + REF_POPUP_CLASS, function () {
					self.$list.empty();
					self.$list.append($.map(references, function (meta, file) {
						return window.mediaRepository.render(file, meta);
					}));
					self.$overlay.removeClass(HIDE_CLASS);

					if(self.scroll) self.scroll.destroy();
					self.scroll = new IScroll(self.$scroll[0], {scrollbars: true});
				});
		}
	};
});
