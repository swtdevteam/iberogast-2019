/**
 * Provides a follow up mail section.
 * ----------------------------------
 * Provides a user interface for composing a follow up email.
 * Focuses on adding and removing media-entries from the media library as attachments.
 *
 * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
 * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
 * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
 *
 * @module ap-follow-up-mail.js
 * @requires jquery.js, media-library.js, agnitio.js, touchy.js
 * @author Alexander Kals, Andreas Tietz, David Buezas, antwerpes ag
 */
app.register('ap-follow-up-mail', function () {
	/**
	 * Implements a follow up mail section.
	 * ------------------------------------
	 * Implements a user interface for composing a follow up email.
	 * Focuses on adding and removing media-entries from the media library as attachments.
	 *
	 * <b>WARNING: Please be responsible and mark only those media entries as attachments whose size
	 * can be handled by common mail servers (using the allowDistribution property on entries within media.json).
	 * Exceeding the mail servers size limitations may result in attachments not being able to be transmitted.</b>
	 *
	 * @class ap-follow-up-mail
	 * @constructor
	 *
	 */
	var self;
	var address = '';
	var subject = 'Reference to our meeting'; // 'Dateien/Referenzen aus unserem Gespräch';
	return {
		publish: {
			allowCustomText: false
		},
		events: {
			'tap .sendButton': 'sendMailHandler',
			'tap .library .emailAttachmentToggler': 'emailAttachmentHandler',
			'tap .clearButton': 'clear'
		},
		states: [
			{
				id: 'visible',
				onEnter: function () {
					app.$.trigger('media-library:refresh');
					app.$.mediaLibrary.refreshScroll();
				}
			}
		],
		onRender: function () {
			self = this;
			app.$.on('open:ap-follow-up-mail', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-follow-up-mail', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			// param data.key - The media repository key to attach
			app.$.on('attach:ap-follow-up-mail', function (data) {
				var el = $('.library .MediaLibrary li[data-file="' + data.key + '"]')[0];
				if (el) {
					window.mediaRepository.toggleAttachment(el);
					self.attachContent(el);
				}
			});
			self.bodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />';
			var presentationName = app.config.get('name');
			localStorage.removeItem(presentationName + ':attachmentStorage');
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-follow-up-mail',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		sendMailHandler: function () {
			// var generalBodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team'; //Sehr geehrter Kunde,<br /><br />anbei finden Sie wie gewünscht die folgenden Dateien/Referenzen aus unserem Gespräch.<br /><br />Mit freundlichen Grüßen,<br />Ihr Bayer Team',
			var presentationName = app.config.get('name');
			var storageNamespace = presentationName + ':attachmentStorage';
			var collectedAttachments = Object.keys(JSON.parse(localStorage[storageNamespace] || '{}'));
			var fileAttachments = [];
			var media = window.mediaRepository.updateAndGetRenderedMetadata(window.mediaRepository.metadata());
			self.bodyText = 'Dear Customer,<br /><br />in reference to our meeting, please find attached the following documents.<br /><br />Best regards<br />Your Bayer team <br /><br /><hr />';
			// Attachments that don't represent real files are writen into the body text,
			// all others are being attached as files:
			$.each(collectedAttachments, function (index, attachment) {
				var contentRegex = /^content:\/\//;
				var urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+|(?:www\.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[.!/\\\w]*))?)/;
				if (contentRegex.test(attachment)) {
					self.bodyText += '<br /><br />';
					self.bodyText += attachment.replace(contentRegex, '');
					// Do we have content to write out?
					if (media[attachment].content) {
						self.bodyText += ': <br /><br />';
						self.bodyText += media[attachment].content;
						self.bodyText += '<hr />';
					}
				} else if (urlRegex.test(attachment)) {
					self.bodyText += '<br /><br />';
					self.bodyText += attachment;
				} else {
					fileAttachments.push(attachment);
				}
			});
			if (self.props.allowCustomText && app.$.customEmailText) {
				self.bodyText += app.$.customEmailText;
			}
			// Hand over to agnitio native mail dialog:
			// eslint-disable-next-line
			console.log('ag.sendMail("' + address + '", "' + subject + '", "' + self.bodyText + '",', fileAttachments, ')');
			ag.sendMail(address, subject, self.bodyText, fileAttachments);
		},
		// param el - The media library list element
		attachContent: function (el) {
			var filename = $(el).attr('data-file');
			var $movedDown = $('.attachments ul li[data-file="' + filename + '"]');
			$('.attachments ul').append($movedDown);
		},
		emailAttachmentHandler: function (event) {
			// var filename = $(event.target).parent('li').attr('data-file');
			self.attachContent($(event.target).parent('li')[0]);
		},
		clear: function () {
			$('.mail [data-file][data-is-attached="true"] .emailAttachmentToggler').each(function () {
				// touchy custom events doesn't support jquery trigger
				var tapEvent = document.createEvent('UIEvents');
				tapEvent.initEvent('tap', true, true);
				this.dispatchEvent(tapEvent);
			});
		}
	};
});
