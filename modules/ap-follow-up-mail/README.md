# ap-follow-up-mail

##### This module provides a user interface for composing a follow up email.

### How to use:

##### Include module to presentation.json
```
"ap-follow-up-mail": {
    "id": "ap-follow-up-mail",
    "files": {
        "templates": [
            "modules/ap-follow-up-mail/ap-follow-up-mail.html"
        ],
        "scripts": [
            "modules/ap-follow-up-mail/ap-follow-up-mail.js"
         ],
         "styles": [
            "modules/ap-follow-up-mail/ap-follow-up-mail.css"
         ]
     }
}
 ```
##### Usage
1) Include the following tag in an ap-toolbar's "content" section
```<div data-module="ap-follow-up-mail" hide></div>```
2) Add the following tag in an ap-toolbar's "bar" section that will trigger a state change
```<div class="button" data-toolbar-state="maximized" data-module-to-load="ap-follow-up-mail"></div>```
