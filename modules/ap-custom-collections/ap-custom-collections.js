/**
 * Implements Custom Collections Editor.
 * -------------------------------------
 *
 * This module allows the edition of custom collections directly on the client (iPad / Browser).
 *
 * @module ap-custom-collections.js
 * @requires jquery.js, iscroll.js, ap-custom-collections-storage.js, ap-overview.js
 * @author David Buezas, antwerpes
 */
app.register('ap-custom-collections', function () {
	var self;
	var eventNamespace = '.custom-collections';
	var _mouseMoveEvent;
	var _mouseUpEvent;
	var currentTouchPos = 0;
	var id;
	var isAnimationFinish = true;
	var daDefaultDataPath = './da-default-data.json';
	var daDefaultData = {};
	return {
		presentationName: null,
		dragData: null,
		publish: {},
		events: {
			'mousedown .o-slide': 'startDragEventHandler',
			'touchstart .o-slide': 'startDragEventHandler',
			'MSPointerDown .o-slide': 'startDragEventHandler',
			'longTouch .overview .o-slide': 'slidePreview',
			'longTouch .edit-zone .o-slide': 'slidePreview',
			'tap .collection-name': function () {
				self.markAlreadyUsedSlides();
			}
		},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function () {
			self = this;
			self.dragData = null;
			$.ajax(daDefaultDataPath)
				.done(function (data) {
					var parsedData = typeof data === 'string' ? JSON.parse(data) : data;
					generateDefaultStructure(parsedData);
				});
			function generateDefaultStructure(data) {
				daDefaultData = data;
				var structures = app.model.get().structures;
				var slideshows = daDefaultData.slideshows.map(function (slideshowId) {
					return structures[slideshowId] || false;
				});
				daDefaultData.slideshows = slideshows;
			}
			function checkContentGroup(contentGroup, slides) {
				return contentGroup.slides.some(function (slide) { return slides.indexOf(slide) > -1; });
			}
			function getSavedContentGroups(slides) {
				var allContentGroups = app.$.contentGroups.json;
				var savedContentGroups = {};
				Object.keys(allContentGroups).forEach(function (key) {
					if (checkContentGroup(allContentGroups[key], slides)) {
						savedContentGroups[key] = allContentGroups[key];
					}
				});
				return savedContentGroups;
			}
			app.$.customCollections = this;
			app.$.on('open:ap-custom-collections', function (data) {
				this.show();
				this.load(data.presentationName);
			}.bind(this));
			app.$.on('close:ap-custom-collections', function () {
				this.hide();
				this.unload();
			}.bind(this));
			// Fix: above listener is never triggered because it's not a 'button'
			// in bottom bar
			// This is called from ap-toolbar module
			app.$.on('close:ap-custom-collections-menu', function () {
				this.hide();
				this.unload();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
			}.bind(this));
			var $editZone = $('.edit-zone');
			$('.cancel').on('tap', function () {
				// user canceled, so go back to the custom collection menu without saving
				self.unload();
				self.hide();
				app.$.overview.hide();
				app.$.customCollectionsMenu.show();
			});
			$('.save').on('tap', function () {
				// save and then go back to the custom collection menu
				var presentation = $.map($editZone.find('.o-slideshow'), function (slideshow) {
					return [$.map($(slideshow).find('.o-slide'), function (slide) {
						return $(slide).attr('data-id');
					})];
				});
				var slides = [];
				$.each(presentation, function (index, slideshows) {
					$.each(slideshows, function (ind, slideId) {
						slides.push(slideId);
					});
				});
				var slideshowArray = [];
				var slideshowIdArray = [];
				var currentChapter = 1;
				$.each(presentation, function (i, slideshow) {
					var slideshowId = 'custom-slideshow-' + Math.floor((Math.random() * 10000) + 1);
					var defaultSlideshow = self._getDefaultSlideshow(slideshow);
					var slideShowName = defaultSlideshow ? defaultSlideshow.name : 'Chapter ' + currentChapter++;
					slideshowIdArray.push(slideshowId);
					var slideshowObj = {id: slideshowId, name: slideShowName, type: 'slideshow', content: slideshow};
					slideshowArray.push(slideshowObj);
					app.model.addStructure(slideshowId, slideshowObj);
				});
				// var homeSlideName = slideshowArray[0].id;
				var existingCollection = app.$.customCollectionsStorage.get(self.presentationName) || {};
				var collectionId = existingCollection.id || null;
				if (!collectionId) {
					collectionId = 'custom-collection-' + Math.floor((Math.random() * 10000) + 1);
				}
				// If updating current slideshow, let's move back to first slide
				// This will prevent presentation to get stuck on removed slide
				if (collectionId === app.slideshow.getId()) {
					app.model.once('update:content', function () {
						app.slideshow.first();
						app.slideshow.trigger('update:anthill-current', {current: {id: app.slideshow.get()}, prev: {id: null}});
					});
				}
				var existingStoryboard = app.model.getStoryboard(collectionId);
				if (existingStoryboard) {
					app.model.updateContent(collectionId, slideshowIdArray);
				} else {
					app.model.addStoryboard(collectionId, {
						id: collectionId,
						name: self.presentationName,
						content: slideshowIdArray,
						type: 'collection'
					});
				}
				var customCollection = {
					id: collectionId,
					name: self.presentationName,
					type: 'collection',
					slideshows: slideshowArray,
					slides: slides,
					presentation: presentation,
					isCustomPresentation: true,
					savedContentGroups: getSavedContentGroups(slides)
				};
				app.$.customCollectionsStorage.add(self.presentationName, customCollection);
				app.$.contentGroups.validateContentgroups();
				self.unload();
				self.hide();
				app.$.overview.hide();
				app.$.customCollectionsMenu.show();
				// Need to update the menu in case current slideshow was changed
				if (app.slideshow.getId() === collectionId) {
					app.$.menu.setup(collectionId);
				}
			});
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-custom-collections',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		getTouch: function (event) {
			var touch;
			if (touchy.isTouch) {
				// 11/16/2017 Updates to custom collections. Making it more stable but does not work in windows
				if (event.originalEvent) {
					// Modern mobile browsers
					if (event.originalEvent.touches) {
						touch = event.originalEvent.touches[currentTouchPos];
					}
				} else if (event.touches) { // Modern mobile browsers
					touch = event.touches[currentTouchPos];
				} else {
					touch = event;
				}
			} else {
				touch = event;
			}
			return touch;
		},
		getTouches: function (event) {
			var touches = [];
			if (event.originalEvent) {
				// Modern mobile browsers
				if (event.originalEvent.touches) {
					touches = Array.from(event.originalEvent.touches);
				}
			} else if (event.touches) { // Modern mobile browsers
				touches = Array.from(event.touches);
			}
			return touches;
		},
		updateCurrentTouchPos: function (event) {
			self.getTouches(event).forEach(function (e, i) {
				if (e.identifier === id) currentTouchPos = i;
			});
		},
		onEnter: function () {},
		onExit: function () {},
		hide: function () {
			app.unlock();
			this.reset();
			this.unload();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		load: function (presentationName) {
			self.presentationName = presentationName;
			_mouseMoveEvent = touchy.events.move + eventNamespace;
			_mouseUpEvent = touchy.events.end + eventNamespace;
			var $document = $(document);
			var slideHTML = '<div class="o-slide"></div>'; // thumbnail template
			var slideshowHTML = '<figure class="o-slideshow"></figure>'; // slideshow template
			var collectionHTML = '<div class="o-collection"></div>'; // collection template
			$('.presentationName').text(presentationName);
			var $editZone = $('.edit-zone');
			var storage = app.$.customCollectionsStorage.get(presentationName);
			// add home and summary slides to the new presentation
			if (storage.presentation.length === 0) {
				Object.keys(daDefaultData).map(function (key) {
					storage[key] = daDefaultData[key];
				});
			}
			function checkImageAddText(src, ele, slide) {
				var image = new Image();
				image.onload = function () {
					if ('naturalHeight' in this) {
						if (this.naturalHeight + this.naturalWidth === 0) {
							this.onerror();
							return false;
						}
					} else if (this.width + this.height === 0) {
						this.onerror();
						return false;
					}
					ele.text('');
					return true;
				};
				image.onerror = function () {
					ele.text(app.model.get().slides[slide].name);
					// eslint-disable-next-line
					console.log('thumbnail not loaded');
					return false;
				};
				image.src = src;
			}
			// create the initial DOM representation from stored presentation
			var $oCollection = $(collectionHTML).append($.map(storage.presentation, function (slideshow) {
				var $slideshow = $(slideshowHTML);
				$slideshow.append($.map(slideshow, function (slideId) {
					var $slide = $(slideHTML).attr('data-id', slideId);
					$slide.css({'background-image': 'url(slides/' + slideId + '/' + slideId + '.png)'});
					var $thumbText = $('<p class="o-text"></p>').appendTo($slide);
					checkImageAddText('slides/' + slideId + '/' + slideId + '.png', $thumbText, slideId);
					$.each(app.$.contentGroups.json, function (groupName, group) {
						if (group.slides.indexOf(slideId) < 0) {
							return;
						} else if (group.slides.indexOf(slideId) === 0) {
							$slide.addClass('first-in-group');
						}
						$slide.data('contentGroup', group);
						$slide.addClass('grouped');
						$slide.addClass(groupName);
						if (group.orientation === 'vertical') {
							$slide.addClass('vertical-group');
						}
						if (group.orientation === 'horizontal') {
							$slideshow.addClass('grouped');
							if (group.slides.indexOf(slideId) === 0) {
								$slideshow.addClass('first-in-group');
							}
						}
					});
					return $slide;
				}));
				return $slideshow;
			}));
			$editZone.append($oCollection);
			self._setupPlaceholders();
			self.markAlreadyUsedSlides();
			var slideWidth = 136;
			var slideHeight = 102;
			app.$.overview.showCustomOverview();
			self.markAlreadyUsedSlides();
			// each time a new collection is selected on the overview, new slide thumbnails are created
			// so we have to re-mark the slides which are already present in the edit zone
			self.scroll = new IScroll($editZone[0], {
				scrollbars: true,
				mouseWheel: true,
				scrollX: true,
				directionLockThreshold: Number.MAX_VALUE
			});
			/*
			 Stores information about the dragged slide while it is being dragged.
			 At runtime it looks like this:
			 {
			 startPos: {
			 x: number,
			 y: number
			 },
			 $clone: (jQ object) draggable slide clone,
			 $original: (jQ object) original slide,
			 currentTarget: DOM element
			 */
			$document.on(_mouseMoveEvent, function (event) {
				// slide comes from the overview and is already present in the edit zone
				// so we forbid it by returning now
				// if drag data is undefined, the user is not dragging anything
				if (event.target.classList.contains('already-used') ||
					!self.dragData) return;

				var touch = self.getTouch(event);
				var target = document.elementFromPoint(touch.pageX, touch.pageY);
				var diffX = touch.pageX - self.dragData.startPos.x;
				var diffY = touch.pageY - self.dragData.startPos.y;
				// If dragged less than 20 pixels, then we don't consider it a drag
				if (Math.abs(diffX) > 20 || Math.abs(diffY) > 20) {
					self.dragData.didDrag = true;
				} else {
					self.dragData.didDrag = false;
				}
				/* Drag Move*/
				self.dragData.$clone.css('transform',
					'translate3d(' +
					(diffX) + 'px,' +
					(diffY) + 'px,' +
					'0' +
					')'
				);
				self.dragData.$clone.css('z-index', '9999');
				/* Drag over */
				var newTarget = null;
				var contentGroup = self.dragData.$original.data('contentGroup');
				if (target && target.classList.contains('o-slide')) {
					// if dragging over a slide, decide which placeholder (over, under, left, right) to mark
					// based on over which quartile the finger (mouse) is
					var $underlyingSlide = $(target);
					var overSlidePos = $underlyingSlide.offset();
					var x = (overSlidePos.left + slideWidth / 2) - touch.pageX;
					var y = (overSlidePos.top + slideHeight / 2) - touch.pageY;
					var angle = Math.atan2(y, -x);
					var PI = Math.PI;
					var $underlyingSlideshow = $underlyingSlide.parent();
					if (angle > -PI / 4 && angle < PI / 4) { // right
						newTarget = $underlyingSlideshow.next('.placeholder')[0];
					} else if (angle > PI / 4 && angle < PI * 3 / 4) { // over
						newTarget = $underlyingSlide.prev('.placeholder')[0];
					} else if (angle > PI * 3 / 4 || angle < -PI * 3 / 4) { // left
						newTarget = $underlyingSlideshow.prev('.placeholder')[0];
					} else if (angle > -PI * 3 / 4 && angle < -PI / 4) { // under
						newTarget = $underlyingSlide.next('.placeholder')[0];
					}
					if (!(contentGroup && contentGroup.orientation === 'horizontal' && $(target).parent('.o-collection').length > 0)) {
						newTarget = null;
					} else {
						$editZone.addClass('highlighted');
					}
					// if (target && target.classList.contains('source')) {
					//  newTarget = target; // TO DO: Make another solution, because this can break.
					// }
				} else if (target && target.classList.contains('placeholder') && self.dragData.didDrag) {
					newTarget = target;
					if (contentGroup && contentGroup.orientation === 'horizontal' && !($(target).parent('.o-collection').length > 0)) {
						newTarget = null;
					}
				}
				if (self.dragData.currentTarget !== newTarget) {
					if (self.dragData.currentTarget) self.dragData.currentTarget.classList.remove('target');
					self.dragData.currentTarget = null;
					if (newTarget && isAnimationFinish) {
						self.dragData.currentTarget = newTarget;
						self.dragData.currentTarget.classList.add('target');
					}
				}
			});
			$document.on(_mouseUpEvent, function () {
				/* Drop */
				// updating a position of a touch related to our slide that is being dragged
				if (event.type === 'touchend') self.updateCurrentTouchPos(event);

				// if drag data is undefined, the user is not dragging anything
				// forbid an execution if touchend came from a touch that is not related to a slide that is being dragged
				if (!self.dragData ||
					event.type === 'touchend' &&
					Array.from(event.touches).map(function (e) {return e.identifier;}).includes(id)) return;

				var dragData = self.dragData;
				dragData.$original.removeClass('source');
				if(isAnimationFinish) {
					if (dragData.currentTarget && dragData.didDrag) {
						/* Drop Insert */
						var $newSlide = dragData.$original.clone(true).off().removeClass('highlighted');
						$(dragData.currentTarget).replaceWith($newSlide);
						var contentGroup = $newSlide.data('contentGroup');
						if (contentGroup) {
							$newSlide.addClass('grouped');
							$newSlide[0].classList.add('first-in-group');
							if (contentGroup.orientation === 'vertical') {
								$newSlide.addClass('vertical-group');
							}
							if (contentGroup && contentGroup.orientation === 'vertical' && !$newSlide.parent().is('.o-slideshow')) {
								$newSlide.wrapAll(slideshowHTML);
							}
						}
						if ($newSlide.parent().is('.o-slideshow')) {
							isAnimationFinish = false;
							$newSlide.css({
								height: 0
							});
							$newSlide
								.animate({
									height: slideHeight
								}, 500, 'easeOutBounce', function () {
									self.scroll.refresh();
									isAnimationFinish = true;
								});
						} else {
							$newSlide.wrap(slideshowHTML);
							var $slideshow = $newSlide.parent();
							$slideshow.css({
								width: 0
							});
							$slideshow
								.animate({
									width: slideWidth
								}, 500, 'easeOutBounce', function () {
									self.scroll.refresh();
								});
						}
						if (contentGroup && contentGroup.orientation === 'horizontal') {
							$newSlide.parents('.o-slideshow').addClass('grouped');
							$newSlide[0].parentNode.classList.add('first-in-group');
						}
					}
					var comesFromCustomPresentation = $editZone.find(dragData.$original).length > 0;
					if (comesFromCustomPresentation && dragData.didDrag) {
						/* Delete original */
						var originalIsAlone = (dragData.$original.siblings('.o-slide').length === 0);
						if (originalIsAlone) {
							var $slideshow_ = dragData.$original.parent();
							var placeholder = $slideshow_.prev('.placeholder');
							placeholder.css('transition', 'none');
							$slideshow_.add(placeholder)
								.animate({
									width: 0
								}, 500, 'easeOutBounce', function () {
									$(this).remove();
									self._setupPlaceholders();
									self.markAlreadyUsedSlides();
									self.scroll.refresh();
								});
						} else {
							var $placeholder = dragData.$original.next('.placeholder');
							$placeholder.css('transition', 'none');
							dragData.$original.add($placeholder)
								.animate({
									height: 0
								}, 500, 'easeOutBounce', function () {
									$(this).remove();
									self._setupPlaceholders();
									self.markAlreadyUsedSlides();
									self.scroll.refresh();
								});
						}
					}
				}
				self.dragData.$clone.remove();
				self._setupPlaceholders();
				self.dragData = null;
				self.markAlreadyUsedSlides();
				self.scroll.enable();
			});
		},
		unload: function () {
			$('.edit-zone').empty();
			app.$.overview.unload();
			self.dragData = null;
			$(document).off(_mouseUpEvent);
			$(document).off(_mouseMoveEvent);
		},
		/**
		 * Adds and removes placeholders as needed.
		 * Thumbnails are dropped into placeholders.
		 * @private
		 * @method _setupPlaceholders
		 * @returns {void}
		 */
		_setupPlaceholders: function () {
			var placeholderHTML = '<div class="placeholder">'; // placeholder template, slides can be dragged into placeholders
			var $editZone = $('.edit-zone');
			var oSlideshowSelector = '.o-slideshow:only-child .o-slide:only-child';
			var oSlideshowFirstChild = '.o-slideshow:first-child';
			var oSlideshowLastChild = '.o-slideshow:last-child';
			// .o-slide+o.slide --> vertically grouped slides
			$editZone.find('.o-slide:first-child,' +
				'.o-slide+.o-slide, .o-slideshow+.o-slideshow')
				.not('.o-slide.grouped+.o-slide.grouped, .o-slideshow.grouped + .o-slideshow.grouped')
				.before(placeholderHTML);
			// console.log('QUERY', $editZone.find('.o-slide+.o-slide'));
			// Add placeholder before horizontal group
			$editZone.find('.o-slideshow.first-in-group')
				.before(placeholderHTML);
			$editZone.find('.first-in-group.vertical-group').before(placeholderHTML);
			$editZone.find('.o-slide:last-child')
				.after(placeholderHTML);
			$editZone.find('.o-collection:empty').append(placeholderHTML);
			// Remove first placeholders
			// $editZone.find('.o-slide.start-group.first-in-group').closest('.o-slideshow').prev('.placeholder').remove();
			// $editZone.find('.o-slide.start-group.first-in-group').next('.placeholder').remove();
			$editZone.find('.placeholder+.placeholder').prev().remove();
			$editZone.find('.o-slideshow > .placeholder:only-child, .o-slideshow:empty').remove();
			$editZone.find('.placeholder+.placeholder').prev().remove(); // must be repeated!

			$editZone.find('.o-collection >' + oSlideshowLastChild).after(placeholderHTML);
			$editZone.find('.o-collection >' + oSlideshowFirstChild).before(placeholderHTML);
			if (this.isInEditZone($editZone, oSlideshowSelector)) {
				$editZone.find(oSlideshowFirstChild).after(placeholderHTML);
			}
		},
		/**
		 * Adds the class 'already-used' to all the overview slides which were added to the edit zone.
		 * This class will forbid marked slides from being added again
		 * @private
		 * @method markAlreadyUsedSlides
		 * @returns {void}
		 */
		markAlreadyUsedSlides: function () {
			var scope = '#' + self.id;
			var $editZone = $('.edit-zone');
			var $overview = $(scope + ' .custom-collections .overview .collection-overview');
			$overview.find('.already-used').removeClass('already-used');
			$editZone.find('.o-slide').each(function () {
				$overview.find('[data-id="start_slide"]').addClass('already-used');
				$overview.find('[data-id="' + $(this).attr('data-id') + '"]').addClass('already-used');
			});
		},
		// Under normal circumstances we would get a custom
		// event from Touchy library.
		// TODO: refactor into an API
		showTooltip: function (event) {
			// console.log('showTooltip event: ', event);
			var el = event.target;
			// Getting coordinates from user's click/tap
			var xPos = event.clientX || event.detail.clientX;
			var yPos = event.clientY || event.detail.clientY;
			var message = 'This slide cannot be added twice';
			// Start slide get's special treatment
			if ($(el).attr('data-id') === 'start_slide') {
				message = 'Start slide cannot be added';
			}
			var d = document.createElement('div');
			d.className = 'o-tip';
			d.innerHTML = message;
			document.body.appendChild(d);
			el.removeEventListener('drag', self.showTooltip);
			// Setting the 'left' and 'top' position from coordinates
			d.style.left = xPos + 'px';
			d.style.top = yPos + 'px';
			// Show the label
			d.style.display = 'block';
			setTimeout(function () {
				document.body.removeChild(d);
			}, 1500);
		},
		isInEditZone: function ($editZone, selector) {
			return $editZone.find(selector).length > 0;
		},
		getOriginalSlideOffsetCompensation: function (group, $originalSlide, originalSlideId) {
			var originalSlideOffsetCompensation = {x: 0, y: 0};
			var isVerticalOrientation = group.orientation === 'vertical';
			var offsetType = isVerticalOrientation ? 'height' : 'width';
			var offset = (group.slides.indexOf(originalSlideId) * $originalSlide[offsetType]()) * -1;
			if (isVerticalOrientation) {
				originalSlideOffsetCompensation.y = offset;
			} else {
				originalSlideOffsetCompensation.x = offset;
			}
			return originalSlideOffsetCompensation;
		},
		getOriginalGroupSlides: function (group, scope) {
			var selector = [];
			$.each(group.slides, function (ind, slide) {
				selector.push(scope + ' .o-slide[data-id="' + slide + '"]');
			});
			return $(selector.join(','));
		},
		_getDefaultSlideshow: function (slideshow) {
			var index = daDefaultData.presentation.findIndex(function (item) {
				return item[0] === slideshow[0];
			});
			return index > -1 ? daDefaultData.slideshows[index] : null;
		},
		startDragEventHandler: function (event) {
			/* Drag Start */
			/* Drag Start */
			// 11/16/2017 Updates to custom collections. Making it more stable but does not work in windows
			// slide comes from the overview and is already present in the edit zone
			// so we forbid it by returning now
			// forbid an mousedown event handling on touch device
			// forbid execution of function on already used slide
			function isMandatorySlide(slideElement) {
				return app.$.customCollectionsStorage.props.mandatory &&  slideElement.classList.contains('defaultSlide');
			}
			if (self.dragData ||
				isLastSlides(event) ||
				touchy.isTouch &&
				event.type === 'mousedown') return;

			function isLastSlides() {
				var isEditZoneAction = $(event.target).parents('.edit-zone').length;
				return isEditZoneAction ? getAllUniqueSlides().length === 1 : false;
			}
			function getAllUniqueSlides() {
				var allSlides = $('.edit-zone').find('.o-slide');
				var filteredSlides = allSlides.filter(function (index, item) {
					return !$(item).hasClass('grouped') || $(item).hasClass('first-in-group');
				});
				return filteredSlides;
			}

			// updating a position of a touch related to our slide that is being dragged
			if (event.type === 'touchstart') {
				id = Array.from(event.targetTouches)[0].identifier;
				self.updateCurrentTouchPos(event);
			}

			var touch = self.getTouch(event);

			if (event.target.classList.contains('already-used')) {
				event.target.addEventListener('drag', self.showTooltip);
				return;
			}
			var selectors = '';
			daDefaultData.slides.map(function (slide, index) {
				selectors += '.custom-collections .edit-zone .o-collection > .o-slideshow .o-slide[data-id="' + slide + '"]';
				if (index < daDefaultData.slides.length - 1) {
					selectors += ', ';
				}
			});
			var defaultSlides = document.querySelectorAll(selectors);
			var dsLength = defaultSlides.length;
			for (var i = 0; i < dsLength; i++) {
				defaultSlides[i].classList.add('defaultSlide');
			}
			// defaultSlides.forEach(function(ds) {
			// 	ds.classList.add('defaultSlide');
			// });
			if (isMandatorySlide(event.target)) return;
			self.scroll.disable();
			var $originalSlide = $(event.target);
			var $clone = $originalSlide.clone().removeClass('highlighted');
			var contentGroups = $originalSlide.parents('.o-collection').data('contentGroups');
			var originalSlideOffsetCompensation = {
				x: 0,
				y: 0
			};
			var contentGroup = $originalSlide.data('contentGroup');
			if (this.isInEditZone($('.edit-zone'), $originalSlide) && contentGroup) {
				$originalSlide = this.getOriginalGroupSlides(contentGroup, '.edit-zone');
				originalSlideOffsetCompensation = this.getOriginalSlideOffsetCompensation(contentGroup, $originalSlide);
				$clone = $originalSlide.clone(true).off();
			} else if (contentGroups) {
				var originalSlideId = $originalSlide.data('id');
				var scope = '#' + this.id;
				$.each(contentGroups, function (groupName, group) {
					if (group.slides.indexOf(originalSlideId) < 0) {
						return;
					}
					originalSlideOffsetCompensation = this.getOriginalSlideOffsetCompensation(group, $originalSlide, originalSlideId);
					$originalSlide = this.getOriginalGroupSlides(group, scope);
					$clone = $originalSlide.clone(true).off();
				}.bind(this));
			}
			self.dragData = {
				startPos: {
					x: touch.pageX + originalSlideOffsetCompensation.x,
					y: touch.pageY + originalSlideOffsetCompensation.y
				},
				$clone: $clone,
				$original: $originalSlide,
				currentTarget: null,
				didDrag: false
			};
			$clone.remove();
			self.dragData.$original.addClass('source');
			var offset = self.dragData.$original.offset();
			self.dragData.$clone
				.css({
					left: offset.left,
					top: offset.top
				});
			// the clone must be inserted at the end of the body, so that it can 'float' over everything else
			// without having to use z-indexes and overflow visibility
			$('body').append(self.dragData.$clone);
			event.preventDefault(); // prevent text selection on desktop
		},
		slidePreview: function (event) {
			event.preventDefault();
			var slide = $(event.target).attr('data-id');
			if (slide) {
				var ele = event.target.outerHTML;
				var eleInPopup;
				app.view.get('ag-overlay').open(ele);
				eleInPopup = document.querySelector('#ag-overlay .o-slide');
				eleInPopup.style.height = '100%';
			}
		}
	};
});
