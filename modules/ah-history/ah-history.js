/**
 * History module
 * -------------------------------------
 *
 * This module triggers event when current view (slide or popup) is updated.
 *
 * @module ah-history.js
 * @requires ag-overlay.js
 * @author Agent Developer
 */
app.register('ah-history', function () {
	return {
		config: {
			debug: true,
			slides: [
				{
					events: {
						target: app.slide,
						enter: 'slide:enter',
						exit: 'slide:exit'
					}
				}, {
					events: {
						target: app,
						enter: 'slideEnter:inlineSlideshow',
						exit: 'slideExit:inlineSlideshow'
					}
				}
			],
			wrappers: [
				{
					events: {
						target: app,
						enter: 'opening:overlay',
						exit: 'closed:overlay'
					}
				}, {
					events: {
						target: app,
						enter: 'opening:inlineSlideshow',
						exit: 'closed:inlineSlideshow'
					}
				}
			]
		},
		onRender: function () {
			this.stack = [];
			this.currentView = null;
			this.previousView = null;
			this.setFutureViewId();
			this.init();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-history',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		init: function () {
			this.config.wrappers.forEach(function (data) {
				app.listenTo(data.events.target, data.events.enter, this.wrapperEnter.bind(this));
				app.listenTo(data.events.target, data.events.exit, this.wrapperExit.bind(this));
			}.bind(this));
			this.config.slides.forEach(function (data) {
				app.listenTo(data.events.target, data.events.enter, this.viewAdd.bind(this));
				app.listenTo(data.events.target, data.events.exit, this.viewRemove.bind(this));
			}.bind(this));
			app.listenTo(app.slideshow, 'update:current', function (data) {
				if (!this.currentView || this.currentView.id !== data.prev.id) {
					this.setFutureViewId(data.current.id);
					this.freeze(this.getInactiveViews(), true);
				}
			}.bind(this));
			app.listenTo(app.slideshow, 'load', function () {
				this.setFutureViewId(app.slideshow.resolve());
				this.freeze(this.getInactiveViews(), true);
			}.bind(this));
			return this._initLogs();
		},
		_initLogs: function () {
			var viewEvents;
			viewEvents = this._getViewConfig().viewEvents;
			app.listenTo(viewEvents.target, viewEvents.enter, function (data) {
				this._log(viewEvents.enter, data);
			}.bind(this));
			app.listenTo(viewEvents.target, viewEvents.exit, function (data) {
				this._log(viewEvents.exit, data);
			}.bind(this));
		},
		_getViewConfig: function () {
			return window.View.prototype.config;
		},
		setFutureViewId: function (futureViewId) {
			this.futureViewId = futureViewId || '';
		},
		getInactiveViews: function () {
			return this.stack.slice(0, this.stack.length - 1);
		},
		freeze: function (stack, isFreeze) {
			stack.forEach(function (view) {
				view.freeze(isFreeze);
			});
		},
		wrapperEnter: function () {
			var current = this.getCurrent();
			if (current) {
				this.trigger('exit', current);
			}
		},
		wrapperExit: function () {
			var current = this.getCurrent();
			if (current) {
				this.trigger('enter', current);
			}
		},
		viewAdd: function (data) {
			var view = new window.View(data.id, data.type);
			this.stack.push(view);
			this.trigger('enter', view);
		},
		viewRemove: function (data) {
			var view = this.getView(data.id);
			if (view) {
				this.stack.splice(this.stack.indexOf(view), 1);
				this.trigger('exit', view);
			}
		},
		getView: function (id) {
			return this.stack.find(function (view) {
				return view.id === id;
			});
		},
		getCurrent: function () {
			return this.stack[this.stack.length - 1];
		},
		dispatch: function (type, view) {
			switch (type) {
			case 'enter':
				if (this.currentView !== view) {
					this.previousView = this.currentView;
					this.currentView = view;
					if (this.previousView) {
						this.previousView.exit();
					}
					this.currentView.enter();
				}
				break;
			case 'exit':
				if (this.currentView === view && this.previousView !== view) {
					this.currentView = null;
					this.previousView = view;
					this.previousView.exit();
				}
				break;
			default:
				break;
			}
		},
		trigger: function (type, view) {
			if (!view.isFreeze) {
				this.dispatch(type, view);
			} else if (view.id === this.futureViewId) {
				this.setFutureViewId();
				this.freeze(this.stack, false);
				this.dispatch(type, view);
			}
		},
		_log: function (message, data) {
			if (this.config.debug) {
				// eslint-disable-next-line
				console.log('%c history log: ' + message, 'font-weight:bold; color: aqua; background: gray;', data);
			}
		}
	};
});
