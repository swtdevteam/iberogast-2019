function View(id, type) {
	this.id = id;
	this.type = type || this.config.viewType;
	this.isFreeze = false;
}

View.prototype.config = {
	viewType: 'slide',
	viewEvents: {
		target: app,
		enter: 'view-enter',
		exit: 'view-exit'
	}
};

View.prototype.freeze = function (isFreeze) {
	this.isFreeze = isFreeze || false;
};

View.prototype.enter = function () {
	this.config.viewEvents.target.trigger(this.config.viewEvents.enter, this);
};

View.prototype.exit = function () {
	this.config.viewEvents.target.trigger(this.config.viewEvents.exit, this);
};
