app.register('ap-notepad', function () {
	/**
	 * This class enables the user to draw over other content via touch/mouse events.
	 *
	 * The module is loaded within index.html
	 *
	 * @class ap-notepad
	 * @constructor
	 *
	 */

	var BRUSH_RED = "assets/brush_red.png";
	var BRUSH_GREEN = "assets/brush_green.png";
	var brush = new Image();
	var sketcher;
	var head;
	var self;

	return {
		publish: {},
		events: {
			'tap .exit': 'toggleNotepad',
			'tap .green': function () {
				brush.src = BRUSH_GREEN;
			},
			'tap .red': function () {
				brush.src = BRUSH_RED;
			},
			'tap .clear': function () {
				sketcher.clear();
			}
		},
		states: [
			{
				id: 'active',
				onEnter: function () {
					app.lock();
					var pencilPos = $('.bar .button.notepad').offset();
					$('.palette').css({
						top: pencilPos.top + 3,
						left: pencilPos.left + 9
					});
				}
			},
			{
				id: 'draw1',
				onEnter: function (prev, data) {
					this.copyDrawing(data);
				}
			},
			{
				id: 'draw2',
				onEnter: function (prev, data) {
					this.copyDrawing(data);
				}
			}
		],
		toggleNotepad: function () {
			var state = this.getState();
			if (state) {
				this.reset();
			} else {
				this.goTo('active');
			}
			sketcher.clear();
		},
		// Methods for copying drawing on remote screen
		copyDrawing: function (data) {
			sketcher.context.putImageData(data, 0, 0);
		},
		updateDrawing: function () {
			var wrapper = app.dom.get('wrapper');
			var imageData = sketcher.context.getImageData(0, 0, wrapper.offsetWidth, wrapper.offsetHeight);
			if (this.stateIs('draw1')) {
				this.goTo('draw2', imageData);
			} else {
				this.goTo('draw1', imageData);
			}
		},
		init: function () {
			brush.src = BRUSH_RED;
			brush.onload = function () {
				sketcher = new window.Sketcher($('#notepad-canvas'), brush);
			};
		},
		onRender: function (el) {
			self = this;
			app.$.notepad = this;
			this.on('reset', app.unlock);
			app.on('updateNotepad', function () {
				self.updateDrawing();
			});
			if (window.Sketcher) {
				this.init();
			} else {
				head.load('modules/ap-notepad/cdm.sketcher.min.js', function () {
					this.init();
				});
			}
			$(el).find('.button:not(.clear,.exit)').on('tap', function (event) {
				$(self.el).find('.button').removeClass('active');
				$(event.target).addClass('active');
			});
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-notepad',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});
