/**
 * Training overlay tour tab group
 * -------------------------------------
 *
 * This module allows to create Tab Group in Tutorial Tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tab-group.js
 * @requires training overlay modules
 * @author Agent Developer
 */
app.register('ah-tab-group', function () {
	return {
		publish: {
			activeClass: 'active',
			tabMenuClass: 'tg-tab',
			tabsSegmentClass: 'tg-content',
			tabDataAttr: 'data-tab'
		},
		states: [],
		_findSegment: function (tabId) {
			return Array.prototype.filter.call(this.tabsSegments, function (tabSegment) {
				return tabSegment.dataset[this._getDatasetProp(this.props.tabDataAttr)] === tabId;
			}.bind(this))[0];
		},
		_getPropsValue: function (el, attr) {
			return el.dataset[this._getDatasetProp(attr)];
		},
		_init: function () {
			this.change = function () {
			};
			this.map = {};
			return Array.prototype.forEach.call(this.tabsMenu, function (tabItem) {
				var tabId;
				tabId = this._getPropsValue(tabItem, this.props.tabDataAttr);
				this.map[tabId] = {
					tabMenu: tabItem,
					tabSegment: this._findSegment(tabId),
					isScroll: false
				};
				tabItem.addEventListener('tap', this.goTo.bind(this, tabId));
			}.bind(this));
		},
		_getDatasetProp: function (attr) {
			return attr.slice(5, attr.length).replace(/-([a-z])/g, function (letter) {
				return letter[1].toUpperCase();
			});
		},
		onRender: function (el) {
			this.wrapper = el;
			this.tabsMenu = this.wrapper.getElementsByClassName(this.props.tabMenuClass);
			this.tabsSegments = this.wrapper.getElementsByClassName(this.props.tabsSegmentClass);
			this._init();
			this.goTo(Object.keys(this.map)[0]);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tab-group',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		initIscroll: function (wrapper) {
			return new IScroll(wrapper, {
				scrollY: true,
				scrollbars: 'custom'
			});
		},
		goTo: function (tabId) {
			if (!tabId && this.map[tabId]) {
				throw Error(tabId + ' is missing');
			}
			if (this.currentTabId && this.currentTabId !== tabId) {
				this.map[this.currentTabId].tabMenu.classList.remove(this.props.activeClass);
				this.map[this.currentTabId].tabSegment.classList.remove(this.props.activeClass);
			}
			this.map[tabId].tabMenu.classList.add(this.props.activeClass);
			this.map[tabId].tabSegment.classList.add(this.props.activeClass);
			if (!this.map[tabId].isScroll) {
				this.initIscroll(this.map[tabId].tabSegment.querySelector('.content-wrap'));
			}
			this.map[tabId].isScroll = true;
			if (this.currentTabId !== tabId) {
				this.change(tabId, this.map[tabId].tabSegment);
			}
			this.currentTabId = tabId;
		}
	};
});
