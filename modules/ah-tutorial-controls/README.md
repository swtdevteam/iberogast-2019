ah-tutorial-controls
======================
This module allows to set button functionality on menu toolbar for training overlay.

## Usage
**This module is part of training overlay and is working in conjunction with "ap-exit-popup", "ah-tab-group", "ah-tour", "ah-tour-configurator" and "ah-tutorial" modules**

- Add the following code to the presentation json file
```
"ah-tutorial-controls": {
    "id": "ah-tutorial-controls",
    "files": {
        "scripts": [
            "modules/ah-tutorial-controls/ah-tutorial-controls.js"
        ],
        "templates": [
            "modules/ah-tutorial-controls/ah-tutorial-controls.html"
        ],
        "styles": [
            "modules/ah-tutorial-controls/ah-tutorial-controls.css"
        ]
    }
}
 ```
- Add the following tag to a desired location on menu toolbar:
```
    <div class="button" data-module='ah-tutorial-controls'></div>
```

## Changelog
### Version 0.0.2 28.09.2018
- Renaming to ah-tutorial-controls, converting coffee to js, stylelint errors fix
### Version 0.0.1 29.12.2017
- Initial creation
