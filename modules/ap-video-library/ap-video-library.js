/**
 * Provides a video library section.
 * ---------------------------------
 * Implements a user interface for viewing media entries that have the "video" tag.
 *
 * @module ap-video-library.js
 * @requires module.js, jquery.js, iscroll.js, media-repository.js
 * @author Andreas Tietz antwerpes ag
 */
app.register('ap-video-library', function () {
	/**
	* Implements a video library section.
	* -----------------------------------
	* Implements a user interface for viewing media entries that have the "video" tag.
	*
	* @class ap-video-library
	* @constructor
	*
	*/
	var self;
	return {
		publish: {},
		events: {},
		states: [
			{
				id: 'visible'
			}
		],
		onRender: function (el) {
			self = this;
			app.$.on('open:ap-video-library', function () {
				if (this.stateIsnt('visible')) {
					this.show();
				}
			}.bind(this));
			app.$.on('close:ap-video-library', function () {
				this.hide();
			}.bind(this));
			app.$.on('toolbar:hidden', function () {
				this.hide();
				this.unload();
			}.bind(this));
			var $list = $(el).find('ul');
			// Initialize scrolling:
			self.scroll = new IScroll(self.$('.scroll')[0], {scrollbars: true});
			// Fill the list with all media entries that have the "video" tag:
			var videos = window.mediaRepository.find('video');
			if (videos) {
				videos = window.mediaRepository.updateAndGetRenderedMetadata(videos);
				$.each(videos, function (file, meta) {
					$list.append(window.mediaRepository.render(file, meta));
				});
				self.scroll.refresh();
			}
			this.monitorUsage();
		},
		onRemove: function () {
			self = null;
		},
		monitorUsage: function () {
			if (window.ag) {
				window.ag.submit.data({
					label: 'Registered Module',
					value: 'ap-video-library',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		hide: function () {
			app.unlock();
			this.reset();
		},
		show: function () {
			app.lock();
			this.goTo('visible');
		},
		unload: function () {
			if (this.stateIs('visible')) {if (self.scroll) self.scroll.destroy();}
		}
	};
});
