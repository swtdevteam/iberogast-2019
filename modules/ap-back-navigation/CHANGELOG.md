### Version 0.0.2 2018-06-28
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 0.0.1 01.06.2018
- Fix of the bug to incorrect writes to history, when we very fast swipe to  slides.
