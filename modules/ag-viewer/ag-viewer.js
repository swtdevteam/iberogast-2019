/* global app */
/* global ag */
app.register('ag-viewer', function () {
	/**
	 * Viewer Module
	 *
	 * This module will open URLs or PDF documents
	 * in iFrame on top of presentation.
	 *
	 * Usage:
	 * - Call ag.openPDF in non-agnitio app or on the web
	 * - Call ag.openURL in non-agnitio app or on the web
	 * - Add 'data-viewer="browser"' to a link (<a>)
	 */

	return {
		template: false,
		publish: {},
		events: {
			'tap .close': 'closeViewer'
		},
		states: [],
		onRender: function () {
			// app.on('ready', this.init.bind(this));

			this.frame = null;
			this.content = [];
			this.inDevice = true;
			this.slideshowOpen = false;

			var info;
			if (window.ag) {
				info = ag.platform.info();
				app.on('openPDF', ag.openPDF.bind(this));
				app.on('openURL', ag.openURL.bind(this));
			}

			// If non-Engager, let's open PDFs in viewer
			if (!app.isVeevaWide) {
				if (!info || (info.localizedModel !== 'iPad' && info.platform !== 'Windows')) {
					this.inDevice = false;
					app.on('openPDF', this.openContent.bind(this));
				}
			}
			app.on('openURL', this.openContent.bind(this));
			app.on('openSlide', this.openSlide.bind(this));
			app.on('openSlideshow', this.openSlideshow.bind(this));

			document.addEventListener('tap', this.handleClick.bind(this));
		},
		onRemove: function () {
			this._removeElement(); // Will undelegate events
		},
		handleClick: function (event) {
			var el = event.target;
			var attr = el.getAttribute('data-viewer') || el.hasAttribute('data-viewer');
			var href = el.getAttribute('href') || attr;
			var slide = el.getAttribute('slide');
			var customClass = el.getAttribute('data-custom-class');
			if (attr) {
				event.preventDefault();
				event.stopPropagation();
				if (attr === 'slide') {
					if (!href) return;
					ag.publish('openSlide', {slide: href});
				} else if (attr === 'slideshow') {
					if (!href) return;
					app.trigger('openSlideshow', {slideshow: href, slide: slide, class: customClass});
				} else if (href) {
					if (/.pdf/.test(href)) {
						ag.openPDF(href);
					} else if (ag.openURL && typeof href === 'string') {
						ag.openURL(href);
					}
				}
			}
		},
		openLink: function (link) {
			var href = link.getAttribute('href');
			if (ag.openURL) ag.openURL(href);
		},
		closeViewer: function () {
			app.unlock();
			var last = this.content.length - 1;
			var view = this.content[last];
			if (view && view.slide) {
				app.slide.remove(view.slide, true);
			}
			if (view && window.viewer.classList.contains('inline-slideshow')) {
				window.viewer.classList.remove('state-ag-overlay-open');
				window.viewer.classList.add('state-default');
				var that = this;
				app.trigger('close:inlineSlideshow');
				window.viewer.classList.remove('loaded');
				window.viewer.classList.remove('visible');
				setTimeout(function () {
					if (that.el) {
						that.el.removeChild(view.container);
					}
					that.frame = null;
					view.container = null;
					that.content.pop();
					that.slideshowOpen = false;
				}, 1000);
			} else if (view) {
				view.container.classList.remove('loaded');
				view.container.classList.remove('visible');
				this.el.removeChild(view.container);
				this.frame = null;
				view.container = null;
				this.content.pop();
			}
		},
		openContent: function (path) {
			var view = {};
			var markup = [
				'<header>',
				'<a class="close" href="#"><span class="icon"></span></a>',
				'</header>',
				'<div class="spinner"></div>',
				'<div class="viewport">',
				'<iframe src="' + path + '"></iframe>',
				'</div>'
			];
			if (!this.inDevice) markup.splice(2, 0, '<a class="external" href="' + path + '" target="_blank"><span class="icon"></span></a>');
			view.container = document.createElement('div');
			view.container.classList.add('preview-link-overlay');
			this.el.appendChild(view.container);
			view.container.innerHTML = markup.join('');
			this.frame = this.el.querySelector('iframe');
			this.frame.addEventListener('load', this.load.bind(this));
			this.content.push(view);
			setTimeout(function () {
				view.container.classList.add('visible');
			}, 1);
			app.lock();
		},
		openSlide: function (data) {
			var viewer = document.createElement('div');
			var view = {};
			viewer.classList.add('viewport');
			// Need to remove slide if already loaded in presentation
			app.slide.remove(data.slide, true);
			view.slide = data.slide;
			view.container = document.createElement('div');
			view.container.classList.add('preview-link-overlay');
			this.el.appendChild(view.container);
			view.container.innerHTML = [
				'<header>',
				'<a class="close" href="#"><span class="icon"></span></a>',
				'</header>',
				'<div class="spinner"></div>'
			].join('');
			app.dom.insert([{id: data.slide}], false, viewer);
			view.container.appendChild(viewer);
			this.content.push(view);
			setTimeout(function () {
				view.container.classList.add('visible');
				view.container.classList.add('loaded');
			}, 1);
			app.lock();
		},
		openSlideshow: function (data) {
			if (this.slideshowOpen) return;
			data.view = {};
			data.view.container = document.createElement('div');
			data.view.container.classList.add('preview-link-overlay');
			data.view.container.innerHTML = [
				'<div class="ag-overlay-background"></div>',
				'<div class="custom-inline-indication">',
				'<button class="prev-slide"></button>',
				'<button class="next-slide"></button>',
				'</div>',
				'<div id="sidebarInline" class="ah-inline-sidebar" data-module="ah-inline-sidebar">',
				'<div class="wrapper-inline-indication">',
				'<div class="inline-indicators"></div>',
				'</div>',
				'</div>',
				'<div class="ag-overlay-x ag-overlay-close close"></div>'
			].join('');
			this.el.appendChild(data.view.container);
			if (data.class) {
				this.el.classList.add(data.class);
			}
			app.trigger('init:inlineSlideshow', {data: data});
			this.content.push(data.view);
			app.trigger('open:inlineSlideshow', {data: data});
			app.lock();
			this.slideshowOpen = true;
		},
		load: function () {
			var last = this.content.length - 1;
			this.content[last].container.classList.add('loaded');
			this.frame.removeEventListener('load', this.load.bind(this));
		}
	};
});
