# ap-toolbar

##### Implements Bottom Toolbar.

##### requires jquery.js, all other classes referenced in toolbar.html

### How to use:

To add buttons to the toolbar add the following to toolbar.html:
```
<div class="button" data-toolbar-state="..." data-module-to-load="..."></div>
```
##### where:
- data-toolbar-state = ["minimized" | "maximized" | "hidden"]
			states the end state of the toolbar when the button is tapped
- data-module-to-load = A class name (must inherit from Module)
			states the class from which an instance will be created and loaded inside the content container
Also add the module to be loaded within the "content"-container
	e.g.:
```
<div data-module="ap-overview" hide></div>
```
If you don't need a module, just remove the button and the entry

add possibility to add pdfs to smpc or popup
for pdf(by default):
    you need remove attributes 'data-ispopup' for example:
        <div class="button" data-toolbar-state="minimized" data-module-to-load="ap-specific-product-characteristics"></div>
for popup:
    you need add attributes 'data-ispopup' for example:
    <div class="button" data-toolbar-state="minimized" data-ispopup data-module-to-load="ap-specific-product-characteristics"></div>
add possibility to fast hide module icons for presentation:
    you need add new item in visibilityConfig:
        for example:
            visibilityConfig = {
                'selector': 'visibility class(disabled or hide)'
            };
            visibilityConfig = {
            	'[data-module-to-load="ap-frequently-asked-questions"]': 'disabled',
            	'[data-module-to-load="ap-custom-collections-menu"]': 'hide',
            	'[data-module-to-load="ap-video-library"]': 'disabled',
            	'[data-module-to-load="ap-follow-up-mail"]': 'hide',
            	'.notepad': 'disabled',
            	'.jumpToLastSlide': 'hide',
            	'[data-module-to-load="ap-reference-library"]': 'disabled'
            };

