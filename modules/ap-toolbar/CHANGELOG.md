### Version 1.0.3 2018-09-28
### Dryzhuk Andrii
- add possibility to fast hide module icons for presentation.

### Version 1.0.2 2018-09-27
### Dryzhuk Andrii
- add possibility to add pdfs to smpc or popup

### Version 1.0.1 2018-07-17
### Oleksandr Melnyk
- fix css linter error
