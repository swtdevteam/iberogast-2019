ag-overlay
============

Module creates an overlay to the presentation.

## Usage

1) Include following tag in master template (index.pug):

```#overlayPopupCustom.custom-popup(data-module='ag-overlay')```

## Settings

The following properties can be set on the above element:

- noBackground BOOLEAN [true] will disable overlay background.
- noCloseBtn BOOLEAN [true] will hide close button.
- content STRING set placeholder on missing html content.
- delay INTEGER delay on unlock presentation.
- type STRING set type of opened content.

### Example

```#overlayPopupCustom.custom-popup(data-module='ag-overlay', noBackground=false, noCloseBtn=false, content='No content available', delay=0, type='popup')```
