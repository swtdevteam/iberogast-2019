### Version 1.0.3 2018-06-28
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 1.0.2 2018-05-31
### Oleksandr Tkachov
- Update code to use current ap-media-repository module functionality, add "metadatafiltrationopts" property which accepts options to perform metadata filtration before it will be rendered.

### Version 1.0.1 2018-04-13
### Vlad Kodenko
- Added span tag to referenceId number wrapper.
