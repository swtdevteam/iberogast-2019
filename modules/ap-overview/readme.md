ap-overview
==============================
This module Presents an overview of the default collections.

## Usage
**This module is working in conjunction with "ap-custom-collections", "ap-custom-collections-menu", "ap-load" and da-default-data.json in app folder**

- Add the following code to the presentation json file
```
    "ap-overview": {
        "id": "ap-overview",
        "ignoreVeeva": true,
        "files": {
            "templates": [
                "modules/rainmaker_modules/ap-overview/ap-overview.html"
            ],
            "scripts": [
                "modules/rainmaker_modules/ap-overview/ap-overview.js"
            ],
            "styles": [
                "modules/rainmaker_modules/ap-overview/ap-overview.css"
            ]
        }
    }
```
