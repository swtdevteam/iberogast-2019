app.register('ah-popup-management', function () {
	var popupsQueue = [];
	var popupOverlay;
	return {
		publish: {
			overlay: 'ag-overlay-for-bhc'
		},
		events: {},
		states: [],
		onRender: function (el) {
			popupOverlay = app.module.get(this.props.overlay);
			app.listenTo(app, 'closed:overlay', this._closePopup.bind(this));
		},
		_openPopup: function (popupId) {
			clearTimeout(this.timeout);
			this.timeout = setTimeout(function () {
				popupOverlay.load(popupId);
			}, popupOverlay.props.delay);
		},
		openPopup: function (popupId) {
			if (popupOverlay.slideId) {
				popupOverlay.close();
			}
			popupsQueue.push(popupId);
			this._openPopup(popupId);
		},
		_closePopup: function (params) {
			if (popupsQueue[popupsQueue.length - 1] === params.slideId) {
				popupsQueue.pop();
				if (popupsQueue.length)
					this._openPopup(popupsQueue[popupsQueue.length - 1]);
			}
		},
		onRemove: function () {
		},
		onEnter: function (el) {
		},
		onExit: function () {
		}
	};
});
