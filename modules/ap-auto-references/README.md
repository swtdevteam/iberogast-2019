# ap-auto-references

##### Implements auto auto-references.

Listens to agnitios 'slide:enter' once and to the 'load'-event, if the slideshow changes
Collects all references made on each loaded slide e.g.
```
   <sup data-reference-id='1'>1</sup>
```
and augments the side-clip on the slide by prepending the fully rendered reference elements.

##### requires jquery.js, auto-side-clip.js, media-repository.js, renderers.js

## Changelog

```
*   v.1.0.1
        2018-07-17 - Oleksandr Melnyk
            refactoring:
                fix css linter errors
                
*   v.1.0.2
        2018-10-09 - Vladyslav Kodenko
            To use updated version add 'injectrefstopopup' flag to the module declaration element in index file
            For updated version use 'ap-auto-references-popup' module to have an ability to open references in popups
            
            update:
                Addition of abilities to:
                    Render references to a referencing element
                    Multiple references addition specifying references or groups of references using comma (default reference separator)
                    Specify sequential group of references using dash (default reference group separator)
```
