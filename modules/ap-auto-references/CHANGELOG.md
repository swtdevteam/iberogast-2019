### Version 0.0.1 2018-06-28
- Initial version

### Version 1.0.2 2018-10-09
- Addition of abilities to:
	- Render references to a referencing element
	- Multiple references addition specifying references or groups of references using comma (default reference separator)
	- Specify sequential group of references using dash (default reference group separator)
