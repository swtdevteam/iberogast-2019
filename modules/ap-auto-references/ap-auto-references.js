/**
 * Implements auto reference popups.
 * ---------------------------------
 *
 * Adds tap handlers to every element that has the [data-reference-id] data attribute.
 * On tap a popup with a list of all references in the slide appears.
 *
 * The module is loaded within index.html
 *
 * @module ap-auto-references-popup.js
 * @requires jquery.js, touchy.js
 * @author Andreas Tietz, antwerpes ag
 */
/**
 * Modified ap-auto-references-popup module to read keys instead of numbers.
 * Grouping them when connected references are used.
 * Adding reference-numbers to the sup-elements
 */


app.register("ap-auto-references", function () {
	var GROUP_SEPARATOR = '-';
	var REF_SEPARATOR = ',';
	var GROUP_SEPARATOR_TO_SHOW = '-';
	var REF_SEPARATOR_TO_SHOW = ',';
	var REF_KEY = 'referenceKey';
	var REF_ATTR = 'data-reference-id';
	var REF_CLASS = 'references';
	var REF_SELECTOR = '[' + REF_ATTR + ']';
	var SIDE_CLIP = 'auto-side-clip';
	var SIDE_CLIP_SELECTOR = '.' + SIDE_CLIP;

	return {
		publish: {},
		events: {},
		states: [],

		onRender: function () {
			if (this.props.injectrefstopopup) {
				app.listenTo(app.slide, 'slide:render',
					this.autoReferencePopupHandlerUpdated.bind(this));
				var slide = app.slideshow.get();
				if(slide)
					this.autoReferencePopupHandlerUpdated({id: slide});
			}
			else {
				app.listenToOnce(app.slide, 'slide:enter', this.autoReferencePopupHandler.bind(this));
				app.listenTo(app.slideshow, 'load', this.autoReferencePopupHandler.bind(this));
			}
		},

		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},

		getReferences: function(refIds, data, refProp = REF_KEY) {
			var media = Object.keys(data)
				.filter(function(e) {return data[e].hasOwnProperty(refProp)})
				.map(function(e) {return String(data[e][refProp])});

			return refIds.split(REF_SEPARATOR).map(function (e) {
				var group = e.split(GROUP_SEPARATOR);
				var res = group;
				if (group.length > 1) {
					var firstGroupIndex = media.indexOf(group[0]);
					var lastGroupIndex = media.indexOf(group[group.length - 1]) + 1;
					res = media.slice(firstGroupIndex, lastGroupIndex);
				}
				return res;
			}).reduce(function(accumulator, e) {
				return accumulator.concat(e)
			}, []);
		},

		getRefindexes: function(reflist, data, refProp = REF_KEY) {
			var refindexes = [];
			$.each(data, function(file, meta) {
				if (reflist.indexOf(String(meta[refProp])) > -1) {
					refindexes.push(meta.referenceId);
				}
			});
			return refindexes;
		},

		getGroups: function (refindexes) {
			var groups = [];
			var point = 0;
			if (refindexes.length > 0) {
				refindexes.reduce(function(prev, next, index) {
					if ((next - prev) !== 1) {
						groups.push(refindexes.slice(point, index));
						point = index;
					}
					return next;
				});
				groups.push(refindexes.slice(point));
			}
			return groups;
		},

		autoReferencePopupHandlerUpdated: function(data) {
			var $slide = $("#" + data.id);
			var self = this;
			var mediaData = window.mediaRepository.metadata();
			var allRefindexes = [];
			$slide.find(REF_SELECTOR).each(function() {
				var referenceIds = $(this).attr(REF_ATTR);
				var reflist = self.getReferences(referenceIds, mediaData);
				var refindexes = self.getRefindexes(reflist, mediaData);
				var groups = self.getGroups(refindexes);

				allRefindexes = allRefindexes.concat(refindexes.filter(
					function (e) {return !allRefindexes.includes(e);}
				));

				$(this)[0].textContent = groups.map(function(group) {
					var result = group;
					if(group.length > 2)
						result = [
							group[0] +
							GROUP_SEPARATOR_TO_SHOW +
							group[group.length - 1]
						];
					return result;
				}).reduce(function(prev, next) {
					return prev.concat(next);
				}).join(REF_SEPARATOR_TO_SHOW);
			});

			app.trigger('refindexes:created', allRefindexes, data.id);
		},

		autoReferencePopupHandler: function() {
			// Load all slide html files of the currently loaded collection
			// and gather all unique reference ids (asynchronously):
			$('article.slide').each(function () {
				var $slide = $(this);
				var referenceIds = {}; // unique reference ids
				$slide.find(REF_SELECTOR).each(function () {
					referenceIds[$(this).attr(REF_ATTR)] = true;
				});
				referenceIds = Object.keys(referenceIds);
				// Find media resources associated with the collected reference ids:
				var references = {};
				$.each(window.mediaRepository.metadata(), function (file, meta) {
					if (referenceIds.indexOf('' + meta.referenceId) > -1) {
						references[file] = meta;
					}
				});
				var $list = $('<ul class=' + REF_CLASS + '/>');
				// Render all references into the list:
				$list.append($.map(references, function (meta, file) {
					return window.mediaRepository.render(file, meta);
				}));
				$slide.find(SIDE_CLIP_SELECTOR).prepend($list);
			});
		}
	}
});
