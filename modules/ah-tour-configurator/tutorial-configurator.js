function TutorialConfigurator(modulesToCheck) {
	this.modulesToCheck = modulesToCheck || [];
	this.popupDelay = 200;
	this.slideDelay = 200;
	this.store = app.module.get('apStorageData');
	this.ahHistory = app.module.get('ahHistory');
	if (app.isVeevaWide) {
		app.once('view-enter', function () {
			if (app.tutorialConfigurator.storageData.isActiveTour) {
				app.module.get('ahTour').openTour();
			} else if (app.tutorialConfigurator.storageData.isActiveTutorial) {
				app.trigger('toggle:tour');
			}
			return this.addHandlers();
		}.bind(this));
		this.initStorageState();
	} else {
		this.addHandlers();
	}
}

TutorialConfigurator.prototype.addHandlers = function () {
	app.on('view-enter', this.setAction.bind(this));
	app.slideshow.on('view-exit', this.close.bind(this));
	app.slide.on('state:enter', this.checkState.bind(this));
	app.slide.on('custom-state:enter', this.checkCustomState.bind(this));
};
TutorialConfigurator.prototype.initStorageState = function () {
	var _storageData = this.store.getStorageData();
	this.storageData = {
		isActiveTutorial: !!_storageData.isActiveTutorial,
		isActiveTour: !!_storageData.isActiveTour,
		isTour: !!_storageData.isTour
	};
	app.listenTo(app, 'tour:show', function () {
		this.storageData.isActiveTour = true;
		this.storageData.isTour = true;
		this.store.save(this.storageData);
	}.bind(this));
	app.listenTo(app, 'tour:hide', function () {
		this.storageData.isActiveTour = false;
		this.store.save(this.storageData);
	}.bind(this));
	app.listenTo(app, 'tour:exit', function () {
		this.storageData.isTour = false;
		this.store.save(this.storageData);
	}.bind(this));
	return app.listenTo(app, 'tutorial:toggle', function (data) {
		this.storageData.isActiveTutorial = data.isActive;
		this.store.save(this.storageData);
	}.bind(this));
};
TutorialConfigurator.prototype.updateState = function () {
	return {
		isActiveTutorial: false,
		isActiveTour: false
	};
};
TutorialConfigurator.prototype.close = function () {
	app.tutorialTour.closeAll();
};
TutorialConfigurator.prototype.checkState = function (data) {
	var slide = app.model.get().slides[data.view];
	if (slide) {
		this.setAction(slide);
	}
};
TutorialConfigurator.prototype.checkCustomState = function (slideId) {
	this.checkState({
		view: slideId
	});
};
TutorialConfigurator.prototype.show = function (event) {
	var element;
	if (event) {
		element = app.dom.get(event.id);
	}
	if (app.tutorialTour) {
		app.tutorialTour.closeAll();
		if (app.tutorialTour.isShow) {
			app.tutorialTour.show(element);
		}
	}
};
TutorialConfigurator.prototype.setAction = function (event) {
	var time = event && (event.type === 'inline' || event.type === 'popup') ? this.popupDelay : this.slideDelay;
	clearTimeout(this.timer);
	this.timer = setTimeout(function () {
		var slideId = this.ahHistory.getCurrent() ? this.ahHistory.getCurrent() : '';
		clearTimeout(this.timer);
		this.show(slideId);
	}.bind(this), time);
};
TutorialConfigurator.prototype.getActiveContainers = function (element) {
	var slideElement = this.getElementWrapper(element);
	var containers = [];
	var modules = this.getActiveElModules();
	if (modules.length > 0) {
		containers = modules;
	} else if (slideElement) {
		containers.push(slideElement);
	}
	return containers;
};
TutorialConfigurator.prototype.getActiveElModules = function () {
	return this.modulesToCheck.filter(function (moduleId) {
		return app.module.get(moduleId);
	}).filter(function (module) {
		return !!module.getState() || module.el.classList.contains('active');
	}).map(function (module) {
		return this.getElementWrapper(module.el);
	}.bind(this));
};
TutorialConfigurator.prototype.getElementWrapper = function (element) {
	if (element && element.parentElement.parentElement.classList.contains('state-ag-overlay-open')) {
		return element.parentElement.parentElement;
	}
	return element;
};
