/**
 * Training overlay tour configuration
 * -------------------------------------
 *
 * This module allows to set Tutorial Tour configuration.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tour-configurator.js
 * @requires tutorial-configurator.js, ah-storage-data, ah-history, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tour-configurator', function () {
	return {
		publish: {},
		modulesToCheck: [],
		onRender: function () {
			app.tutorialConfigurator = new window.TutorialConfigurator(this.modulesToCheck);
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tour-configurator',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		}
	};
});
