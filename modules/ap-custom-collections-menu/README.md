ap-custrom-collections-menu
======================
This module allows the creation / naming / deletion and loading of custom collections.

## Usage
**This module is working in conjunction with "ap-custom-collections", "ap-overview", "ap-load" and da-default-data.json in app folder**

- Add the following code to the presentation json file
```
"ap-custom-collections-menu": {
         "id": "ap-custom-collections-menu",
         "ignoreVeeva": true,
            "files": {
                "templates": [
                     "**SOME-DIRECTORY**/ap-custom-collections-menu/ap-custom-collections-menu.html"
                ],
                "scripts": [
                   "**SOME-DIRECTORY**/ap-custom-collections-menu/ap-custom-collections-menu.js"
                ],
                "styles": [
                   "**SOME-DIRECTORY**/ap-custom-collections-menu/ap-custom-collections-menu.css"
                ]
            }
       }
 ```
- Add the following tag to a desired location with the "ap-custom-collections", "ap-overview", "ap-load" modules already placed
```
<div data-module="ap-custom-collections-menu" hide ></div>
```
