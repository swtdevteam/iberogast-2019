/**
 * Training overlay tour functionality
 * -------------------------------------
 *
 * This module allows to create Tutorial Tour.
 * Training overlay modules:
 *    ap-exit-popup
 *    ah-tab-group
 *    ah-tour
 *    ah-tour-configurator
 *    ah-tutorial
 *    ah-tutorial-controls
 *
 * @module ah-tour.js
 * @requires tutorial-tour.js, knockout.js, bootstrap.min.js, ah-history, training overlay modules
 * @author Serhii.Zv, Mykola Fant
 */
app.register('ah-tour', function () {
	return {
		onRender: function (el) {
			this.$el = el;
			this.activeTour = window.ko.observable();
			this.isOpened = window.ko.observable();
			this.history = app.module.get('ahHistory');
			this.$closeButton = this.createCloseButton();
			this.$el.appendChild(this.$closeButton);
			app.tutorialTour = this.createTutorialTour();
			this.$closeButton.addEventListener('tap', this.exitTour.bind(this));
			this.initListeners();
			this.monitorUsage();
		},
		monitorUsage: function () {
			if (window.ag) {
				ag.submit.data({
					label: 'Registered Module',
					value: 'ah-tour',
					category: 'BHC Template Modules',
					isUnique: true,
					labelId: 'bhc_registered_module',
					categoryId: 'bhc_template_modules'
				});
			}
		},
		createTutorialTour: function () {
			return new window.TutorialTour({
				viewport: {
					selector: '#presentation',
					padding: 10
				},
				getActiveContainers: app.tutorialConfigurator.getActiveContainers.bind(app.tutorialConfigurator)
			});
		},
		initListeners: function () {
			app.on('toggle:tour', this.toggleTour.bind(this));
		},
		createCloseButton: function () {
			var button = document.createElement('button');
			button.classList.add('close-tour');
			return button;
		},
		exitTour: function () {
			this.activeTour(false);
			this.closeTour();
			this.trigger('tour:exit', {});
		},
		toggleTour: function () {
			if (this.isOpened()) {
				this.closeTour();
			}
			else {
				if (app.isVeevaWide) {
					if (app.tutorialConfigurator.storageData.isTour) {
						this.openTour();
					}
				} else if (this.activeTour()) {
					this.openTour();
				}
			}
		},
		trigger: function (eventName, data) {
			app.trigger(eventName, data);
		},
		openTour: function () {
			this.activeTour(true);
			this.isOpened(true);
			app.tutorialTour.show(app.dom.get(this.history.getCurrent().id));
			this.$el.classList.add('show');
			this.trigger('tour:show', {});
		},
		closeTour: function () {
			this.isOpened(false);
			app.tutorialTour.hide();
			app.tutorial.toggleTutorial();
			this.$el.classList.remove('show');
			this.trigger('tour:hide', {});
		}
	};
});
