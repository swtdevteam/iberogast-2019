/**
 * Implements Custom Collections Storage.
 * -------------------------------------
 * This module manages the storage (localStorage) for custom collections.
 *
 * The module is loaded within index.html
 *
 * @module ap-custom-collections-storage.js
 * @requires jquery.js
 * @author David Buezas, antwerpes
 */
app.register('ap-custom-collections-storage', function () {
	/**
	 * Implements Custom Collections Storage.
	 * -------------------------------------
	 * This class manages the storage (localStorage) for custom collections.
	 *
	 * @class ap-custom-collections-storage
	 * @constructor
	 */
	var self;
	var translation;
	return {
		publish: {
			mandatory: false
		},
		events: {},
		onRender: function () {
			if (app.config.get('name') === 'Bayer Health Care Rainmaker Template (replace with the name of your presentation)') {
				// eslint-disable-next-line
				console.error('### Please update the name of the presentation in the config.json ###');
			}
			this.storageNamespace = this.getStorageNamespace();
			self = this;
			app.$.customCollectionsStorage = this;
		},
		/**
		* returns storageNamespace
		*
		* @method getStorageNamespace
		* @returns {String} - returns storageNamespace
		*
		*/
		getStorageNamespace: function () {
			return app.config.get('name') + ':' + app.model.getStoryboard()[0] + ':CustomCollections';
		},
		/**
		 * Deletes the custom collection named collectionName from the localStorage
		 *
		 * @method delete
		 * @param {string} collectionName - collection name
		 * @returns {void} - returns nothing
		 */
		delete: function (collectionName) {
			var customCollections = self.getAll();
			var current = app.slideshow.getId();
			// eslint-disable-next-line
			console.log('Delete collection', customCollections[collectionName]);
			if (customCollections[collectionName] !== current) {
				delete customCollections[collectionName];
				app.model.deleteStoryboard(collectionName);
				localStorage[self.storageNamespace] = JSON.stringify(customCollections);
			} else {
				// alert();
			}
		},
		/**
		 * returns all custom collections as an associative array from collection name to collection representation
		 *
		 * @method getAll
		 * @returns {void} - returns nothing
		 */
		getAll: function () {
			try {
				return JSON.parse(localStorage[self.storageNamespace] || '{}');
			} catch (e) {
				// eslint-disable-next-line
				console.log('Custom Presentations storage is corrupt or empty and will be reseted: ' + JSON.stringify(e));
				delete localStorage[self.storageNamespace];
			}
			return {};
		},
		/**
		 * Adds a custom collection to the localStorage
		 *
		 * @method add
		 * @param {string} collectionName name of the collection to store
		 * @param {string} collectionObject object representation of the collection
		 * @returns {void} - returns nothing
		 */
		add: function (collectionName, collectionObject) {
			var customCollections = self.getAll();
			customCollections[collectionName] = collectionObject;
			localStorage[self.storageNamespace] = JSON.stringify(customCollections);
		},
		/**
		 * returns the stored collection with the provided name
		 *
		 * @method get
		 * @param {string} collectionName name of the collection to store
		 * @returns {object} - collection returned
		 */
		get: function (collectionName) {
			var collection = self.getAll()[collectionName];
			if (collection === void 0 || Array.isArray(collection.presentation) === false) {
				return void 0;
			}
			var invalidSlidesCount = 0;
			collection.presentation = collection.presentation.map(function (slideshow) {
				return slideshow.filter(function (slideId) {
					if (app.model.get().slides[slideId] === void 0) {
						invalidSlidesCount++;
						return false;
					}
					return true;
				});
			}).filter(function (slideshow) {
				return slideshow.length > 0;
			});
			if (invalidSlidesCount > 0) {
				if (translation) {
					var errorMsg = translation.MISSING_PRESSENTATIONS_TITLE
						.replace('$INVALID_SLIDES_COUNT$', invalidSlidesCount);
					window.apprise(errorMsg, {textOk: translation.OK});
				}
				self.add(collectionName, collection);
			}
			return collection;
		},
		/**
		 * renames a stored collection without changing its position (e.g. collections order returned by getAll)
		 *
		 * @method rename
		 * @param {string} oldName - old name
		 * @param {string} newName - new name
		 * @returns {void} - returns nothing
		 */
		rename: function (oldName, newName) {
			// rename presentation keeping the presentation order
			var customCollections = {};
			$.each(self.getAll(), function (name, object) {
				if (name === oldName) {
					$.each(object.slideshows, function ($name, slideshow) {
						slideshow.name = slideshow.name.replace(oldName, newName);
					});
					object.name = newName;
				}
				customCollections[(name === oldName) ? newName : name] = object;
			});
			// var isFavorite = customCollectionsStorage.isFavorite(oldName);
			// customCollectionsStorage.isFavorite(oldName, false);
			// customCollectionsStorage.isFavorite(newName, isFavorite);
			localStorage[self.storageNamespace] = JSON.stringify(customCollections);
		},
		/**
		 * returns all stored favorite collections
		 *
		 * @method getFavorites
		 * @returns {void} - returns nothing
		 *
		 */
		getFavorites: function () {
			var collections = self.getAll();
			var favorites = [];
			$.each(collections, function (collectionName, collectionObject) {
				if (collectionObject.isFavorite) {
					favorites.push(collectionObject);
				}
			});
			return favorites;
		},
		/**
		 * returns all stored favorite collections
		 *
		 * @method updateOrder
		 * @param {Array} order - order
		 * @returns {void} - returns nothing
		 *
		 */
		updateOrder: function (order) {
			var customCollections = self.getAll();
			var newCustomCollections = {};
			// clear old order
			var backUp = localStorage[self.storageNamespace];
			localStorage[self.storageNamespace] = JSON.stringify({});
			// set new order
			try {
				$.each(order, function (index, name) {
					newCustomCollections[name] = customCollections[name];
				});
			} catch (e) {
				localStorage[self.storageNamespace] = backUp;
			}
			localStorage[self.storageNamespace] = JSON.stringify(newCustomCollections);
		},
		/**
		 * store all custom-collections
		 *
		 * @method saveAll
		 * @param {string} customPresentations - custom presentations
		 * @returns {void} - returns nothing
		 *
		 */
		saveAll: function (customPresentations) {
			localStorage[self.storageNamespace] = JSON.stringify(customPresentations);
		}
	};
});
