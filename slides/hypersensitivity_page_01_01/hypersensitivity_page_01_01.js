app.register("hypersensitivity_page_01_01", function() {

    var mainClass = "#hypersensitivity_page_01_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        $(mainClass).find(".tabcontent.one").show();
        $(mainClass).find(".tabcontent.two").hide();
        $(mainClass).find(".btn-tab.efficacy").addClass("active");

        $(mainClass).find(".btn-tab.efficacy").click(function(){
            // $(".btn-tab.efficacy").show();
            // $(".btn-tab.tolerability").hide();
            $(mainClass).find(".tabcontent.one").show();
            $(mainClass).find(".tabcontent.two").hide();
            $(mainClass).find(".btn-tab.tolerability").removeClass("active");
            $(mainClass).find(".btn-tab.efficacy").addClass("active");
        });
        $(mainClass).find(".btn-tab.tolerability").click(function(){
            // $(".btn-tab.tolerability").show();
            // $(".btn-tab.efficacy").hide();
            $(mainClass).find(".tabcontent.one").hide();
            $(mainClass).find(".tabcontent.two").show();
            $(mainClass).find(".btn-tab.efficacy").removeClass("active");
            $(mainClass).find(".btn-tab.tolerability").addClass("active");
        });
              
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});