app.register("master_detailer_page_03_02", function() {

    var mainClass = "#master_detailer_page_03_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        $(mainClass).find(".tabcontent.one").show();
        $(mainClass).find(".tabcontent.two").hide();
        $(mainClass).find(".btn-tab.efficacy").addClass("active");

        $(mainClass).find(".btn-tab.efficacy").click(function(){
            // $(".btn-tab.efficacy").show();
            // $(".btn-tab.tolerability").hide();
            $(mainClass).find(".tabcontent.one").show();
            $(mainClass).find(".tabcontent.two").hide();
            $(mainClass).find(".btn-tab.tolerability").removeClass("active");
            $(mainClass).find(".btn-tab.efficacy").addClass("active");
        });
        $(mainClass).find(".btn-tab.tolerability").click(function(){
            // $(".btn-tab.tolerability").show();
            // $(".btn-tab.efficacy").hide();
            $(mainClass).find(".tabcontent.one").hide();
            $(mainClass).find(".tabcontent.two").show();
            $(mainClass).find(".btn-tab.efficacy").removeClass("active");
            $(mainClass).find(".btn-tab.tolerability").addClass("active");
        }); 
              
            // for Popup Animation
            $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(450, function() {
                    var time =350;
                    var setTime =750;
   
                        $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 1000);
                        $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);                    
                });

                e.preventDefault();
            });

            $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(450, function() {

                    $(mainClass + "[class*='ani-'] ").hide(); 

                });

                e.preventDefault();
            });
            // end for Popup Animation

    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    // Set times for each animation
    // var time =350;
    // var setTime =750;
    
    //     $(mainClass + ".ani-1").delay(0).show( "slide",{direction: "down"}, 1000);
    //     $(mainClass + ".ani-2").delay(time+=setTime).show( "slide",{direction: "down"}, 1000);

    },
    onExit: function(el) {

        //$(mainClass + "[class*='ani-'] ").hide(); 
    }
  }

});