app.register("hypersensitivity_page_01_02", function() {

    var mainClass = "#hypersensitivity_page_01_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

            // for Popup
            $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(450);

                e.preventDefault();
            });

            $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(450);

                e.preventDefault();
            });         

    },
    onExit: function(el) {

    }
  }

});