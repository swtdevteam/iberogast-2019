app.register("secretion_page_02_01", function() {

    var mainClass = "#secretion_page_02_01 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        $(mainClass).find(".content-one").show();
        $(mainClass).find(".content-two").hide();
        
        $(mainClass).find("#hide").click(function(){
            $(mainClass).find(".btn-next.ref").show();
            $(mainClass).find(".btn-back.ref").hide();
            $(mainClass).find(".content-one").show();
            $(mainClass).find(".content-two").hide();
        });
        $(mainClass).find("#show").click(function(){
            $(mainClass).find(".btn-back.ref").show();
            $(mainClass).find(".btn-next.ref").hide();
            $(mainClass).find(".content-one").hide();
            $(mainClass).find(".content-two").show();
        });

    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

            // for Popup
            $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(450);

                e.preventDefault();
            });

            $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(450);

                $("video").each(function () { 
                    this.pause() ;
                });

                e.preventDefault();
            }); 

    },
    onExit: function(el) {

        $("video").each(function () { 
            this.pause() ;
        });
        
    }
  }

});