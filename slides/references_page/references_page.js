app.register("references_page", function() {

    var mainClass = "#references_page ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        $(mainClass).find(".gadovist-one").show();
        //$(mainClass).find(".gadovist-one").addClass("animated fadeIn");

        $(mainClass).find("#hide").click(function(){
            $(mainClass).find(".btn-next.ref").show();
            $(mainClass).find(".btn-back.ref").hide();
            $(mainClass).find(".gadovist-one").show();
            $(mainClass).find(".gadovist-two").hide();
        });
        $(mainClass).find("#show").click(function(){
            $(mainClass).find(".btn-back.ref").show();
            $(mainClass).find(".btn-next.ref").hide();
            $(mainClass).find(".gadovist-one").hide();
            $(mainClass).find(".gadovist-two").show();
        });       
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

       

        //$(".gadovist-one").addClass("animated slideInLeft");
        //$(mainClass).find(".gadovist-two").addClass("animated fadeIn");

            // for Popup
            $('[data-popup-open]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-open');
                $('[data-popup="' + targeted_popup_class + '"]').fadeIn(450);

                e.preventDefault();
            });

            $('[data-popup-close]').on('click', function(e)  {
                var targeted_popup_class = jQuery(this).attr('data-popup-close');
                $('[data-popup="' + targeted_popup_class + '"]').fadeOut(450);

                $("video").each(function () { 
                    this.pause() ;
                });

                e.preventDefault();
            }); 
        
    },
    onExit: function(el) {

        $("video").each(function () { 
            this.pause();
        });
    }
  }

});