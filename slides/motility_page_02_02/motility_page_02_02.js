app.register("motility_page_02_02", function() {

    var mainClass = "#motility_page_02_02 ";

  return {
    events: {

    },
    states: [],
    onRender: function(el) {

        $(mainClass).find(".content-one").show();
        $(mainClass).find(".content-two").hide();
        
        $(mainClass).find("#hide").click(function(){
            $(mainClass).find(".btn-next.ref").show();
            $(mainClass).find(".btn-back.ref").hide();
            $(mainClass).find(".content-one").show();
            $(mainClass).find(".content-two").hide();
        });
        $(mainClass).find("#show").click(function(){
            $(mainClass).find(".btn-back.ref").show();
            $(mainClass).find(".btn-next.ref").hide();
            $(mainClass).find(".content-one").hide();
            $(mainClass).find(".content-two").show();
        });        
      
    },
    onRemove: function(el) {
        
    },
    onEnter: function(el) {

    },
    onExit: function(el) {

    }
  }

});