Bayer Health Care Template - Rainmaker

### Application Info :

- ## ID/NAME = "17473 / Bayer Health Care Template"
- ## CLM system = "Rainmaker"

- ## ID/NAME = "107841 / Bayer Health Care Template"
- ## CLM system = "Iplanner"

- ## ID/NAME = "105988 / test - Bayer Health Care Template"
- ## CLM system = "Iplanner"

Setup:

npm install agnitio-cli -g

Run:

agnitio run

Build:

agnitio build

See changelog.pdf document for more info.
